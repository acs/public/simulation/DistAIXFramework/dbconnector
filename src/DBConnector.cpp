/**
 * This file is part of DistAIX DBconnector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/


#include "DBConnector.h"
#include <libpq-fe.h> 
#include <string>
#include <cstring>
#include <iostream>
#include <chrono>
#include "DBException.h"
#include <typeinfo>
#include <queue>

void cassandraInsertCallback(CassFuture* future, void* data);

/*!
*   \brief Register a new agent with the Cassandra DB
 *  \param cass_db name of the cassandra db
 *  \param session pointer to cassandra session
 *  \param simId Id of simulation to which agent shall be added
*   \param agentId ID of the agent to be registered
* */
void addAgentToCassandra(std::string cass_db, CassSession* session, long simId,  std::string agentId){
    std::string query = "INSERT INTO ";
    query+=cass_db;
    query+=".agents (simid, agentid) VALUES (?, ?)";

    CassStatement* statement= cass_statement_new(query.c_str(), 2);

    cass_statement_bind_int32(statement, 0, simId);
    cass_statement_bind_string(statement, 1, agentId.c_str());

    CassFuture* query_future = cass_session_execute(session, statement);
    /* Statement objects can be freed immediately after being executed */
    cass_statement_free(statement);

    if (cass_future_error_code(query_future) != CASS_OK) {
        const char* error_message;
        size_t error_message_length;
        std::cerr << "Cassandra error at adding agent " << agentId << std::endl;
        cass_future_error_message(query_future, &error_message, &error_message_length);
        std::string msg = "Could not insert data: ";
        msg += error_message;
        throw DBException(msg.c_str());
    }

    cass_future_free(query_future);
}


void* cassandra_insert_thread_func(void* arg){
    try {
        // extract thread parameters
        std::queue<cass_dataset*> *cassandra_insert_queue = ((cassinfo *) arg)->cassandra_insert_queue;
        long *insertCount_pointer = ((cassinfo *) arg)->insertCount_pointer;
        const CassPrepared **prepared = ((cassinfo *) arg)->prepared;
        bool *finished = ((cassinfo *) arg)->finished;
        std::string *cass_db = ((cassinfo *) arg)->cass_db;
        CassSession **session = ((cassinfo *) arg)->session;
        void *pointer_to_dbconn = ((cassinfo *) arg)->pointer_to_dbconn;
        std::mutex *insertQueueMutex = ((cassinfo *) arg)->insertQueueMutex;
        std::mutex *insertCountMutex = ((cassinfo*) arg)->insertCountMutex;

        insertQueueMutex->lock();
        bool empty = cassandra_insert_queue->empty();
        insertQueueMutex->unlock();

        // while not finished or something is in the queue
        while (!(*finished) || !empty) {
            if (!empty) {
                insertQueueMutex->lock();
                //get oldest element in queue and remove it from queue
                cass_dataset *data = cassandra_insert_queue->front();
                cassandra_insert_queue->pop();
                insertQueueMutex->unlock();

                if (data->isFirst) { //add agent to agents table
                    addAgentToCassandra(*cass_db, *session, data->simId, data->agentId);

                }
                CassStatement *statement = cass_prepared_bind(*prepared);

                cass_statement_bind_int32_by_name(statement, "simid", data->simId);
                cass_statement_bind_string_by_name(statement, "agentid", data->agentId.c_str());
                cass_statement_bind_int32_by_name(statement, "agenttypeid", data->agentTypeId);
                cass_statement_bind_double_by_name(statement, "simtime", data->simTime);
                cass_statement_bind_bytes_by_name(statement, "result", (const cass_byte_t *) data->data, data->size);

                CassFuture *result_future = cass_session_execute(*session, statement);
                cass_future_set_callback(result_future, cassandraInsertCallback, pointer_to_dbconn);

                cass_future_free(result_future);
                cass_statement_free(statement);
                free(data->data); // free memory of buffer data allocated in addResultSerialized
                delete data; // free memory of cassandra data set

                insertCountMutex->lock();
                (*insertCount_pointer)++; // increment counter of inserts
                insertCountMutex->unlock();
            }
            insertQueueMutex->lock();
            empty = cassandra_insert_queue->empty();
            insertQueueMutex->unlock();
        }
    } catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in cassandra_insert_thread_func(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in cassandra_insert_thread_func(...): "  << ex.what() << std::endl;
    }

    return nullptr;
}


/*!
*   \brief create a new DBConnector
*   \param _host      postgres host
*   \param _username  postgres username
*   \param _password  postgres password
*   \param _database  name for database which should be used, doesn't need to exist, will be created if not with initialize()
* */
DBConnector::DBConnector(std::string _host, std::string _username, std::string _password, int _port, std::string _database){
  username = _username;
  host     = _host;
  password = _password;
  port     = _port;
  database = _database;
  conn = PQconnectdb(("dbname=postgres user="+username+" password="+password+" hostaddr="+host+" port="+ std::to_string(port)).c_str()); 
  if(PQstatus(conn) != CONNECTION_OK){
    throw DBException(std::string("Connection to DB failed")+PQerrorMessage(conn));
  }

  finished = false;
  cassandra_info_for_thread.prepared = &prepared;
  cassandra_info_for_thread.insertCount_pointer = & insertCount;
  cassandra_info_for_thread.cassandra_insert_queue = &cassandra_insert_queue;
  cassandra_info_for_thread.finished = &finished;
  cassandra_info_for_thread.cass_db = &cass_db;
  cassandra_info_for_thread.session = &session;
  cassandra_info_for_thread.pointer_to_dbconn = this;
  cassandra_info_for_thread.insertQueueMutex = &insertQueueMutex;
  cassandra_info_for_thread.insertCountMutex = &insertCountMutex;
  pthread_create(&cassandra_insert_thread, nullptr, cassandra_insert_thread_func, &cassandra_info_for_thread);
}


/**
*CASSANDRA 
**/


/*!
*   \brief Increase the counter of cassandra callbacks
* */
void DBConnector::increaseCassandraCallbackCount(){
  callbackCountMutex.lock();
  callbackCount++;
  callbackCountMutex.unlock();
}

/*!
*   \brief Set configuration parameters of the Cassandra DB
 *  \param _hosts String list of host IPs separated by comma
 *  \param _dbname Name of Cassandra data base
 *  \param _iothreads Number of IO threads used by Cassandra
* */
void DBConnector::setCassandraConfig(std::string _hosts, std::string _dbname, int _iothreads){
  cass_hosts = _hosts;
  cass_db = _dbname;
  iothreads = _iothreads;
}


/*!
*   \brief creates a new simulation
*   \return the new simulation ID
* */
int DBConnector::newSim(){
    int ret = 0;
    try {

        PGresult *r = PQexec(conn, "INSERT INTO simulations(start_ts) VALUES(NOW()) RETURNING simulationid");
        checkPostgreSQLSelectError(r, "newSim");
        ret = std::stoi(PQgetvalue(r, 0, 0));
        PQclear(r);

    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in newSim(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in newSim(...): "  << ex.what() << std::endl;
    }
    return ret;
}

/*!
*   \brief creates a new resulttype to save additional information for a result
*   \param name name as used in the protobuf file
*   \param unit unit of result
*   \param additional description for resulttype
* */
void DBConnector::addResultType(std::string name, std::string unit, std::string description){
    try {
        std::string s = "INSERT INTO resulttypes(resultType, simulationid, unit, description) VALUES('" + name + "'," +
                        std::to_string(simId) + ",'" + unit + "','" + description + "') ON CONFLICT DO NOTHING";
        PGresult *r = PQexec(conn, s.c_str());
        checkPostgreSQLError(r, "addResultType_insertType");
        PQclear(r);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in addResultType(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in addResultType(...): "  << ex.what() << std::endl;
    }
}

/*!
*   \brief creates a new type for time measurements
*   \param mt a name for the time measurement
*   \param description additional description for resulttype
* */
void DBConnector::addTimeMeasurementType(std::string mt, std::string description){
    try {
        PGresult *r = PQexec(conn,
                             ("SELECT COUNT(timeMeasurementType) FROM timeMeasurementTypes WHERE timeMeasurementType='" +
                              mt + "'").c_str());
        checkPostgreSQLSelectError(r, "addTimeMeasurementType_countTypes");
        int c = std::stoi(PQgetvalue(r, 0, 0));
        PQclear(r);
        // if the count is zero, the measurementType doesnt exist and needs to be insert
        if (c == 0) {
            r = PQexec(conn,
                       ("INSERT INTO timeMeasurementTypes(timeMeasurementType, description) VALUES('" + mt + "','" +
                        description + "')").c_str());
            checkPostgreSQLError(r, "addTimeMeasurementType_addType");
            PQclear(r);
        }
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in addTimeMeasurementType(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in addTimeMeasurementType(...): "  << ex.what() << std::endl;
    }

}

/*!
*   \brief sets the stop time in the simulations table for the current simulation
* */
void DBConnector::finishSimulation(){
    try {
        PGresult *r = PQexec(conn, ("UPDATE simulations set stop_ts=NOW() WHERE simulationId=" +
                                    std::to_string(simId)).c_str());
        checkPostgreSQLError(r, "finishSimulation");
        PQclear(r);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in finishSimulation(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in finishSimulation(...): "  << ex.what() << std::endl;
    }
}


/*
adding results/metadata/timemeasurements with COPY
*
*/

/*!
*   \brief wrapper for same named function where agentID is a string
*   \param metatype name for metadata entry 
*   \param agentId agentId which the metadata belongs to
*   \param agentTypeId agentTypeId which the metadata belongs to
*   \param value data
* */
void DBConnector::addAgentMetaEntry(std::string metatype, long agentId, long agentTypeId, double value){
    addAgentMetaEntry(metatype, std::to_string(agentId), agentTypeId, value);
}

/*!
*   \brief adds a metadata entry for an agent to the database
*   \param metatype name for metadata entry 
*   \param agentId agentId which the metadata belongs to
*   \param agentTypeId agentTypeId which the metadata belongs to
*   \param value data
* */
void DBConnector::addAgentMetaEntry(std::string metatype, std::string agentId, long agentTypeId, double value){
    try {
        // create strings, allocate mem
        std::string currentTs_str = std::to_string(getCurrentTs());
        std::string agentTypeId_str = std::to_string(agentTypeId);
        std::string value_str = std::to_string(value);

        std::string buffer = std::to_string(simId)
                           + '\t' + metatype
                           + '\t' + agentId
                           + '\t' + agentTypeId_str
                           + '\t' + value_str + '\n';
        int r = PQputCopyData(copy_r_cn, buffer.data(), buffer.length());
        checkPostgreSQLCopyError(r, copy_r_cn, "addAgentMetaEntry");
        resultCount++;
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in addAgentMetaEntry(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in addAgentMetaEntry(...): "  << ex.what() << std::endl;
    }

}

/*!
*   \brief add an meta entry for a simulation
*   \param id ID of the simulation
*   \param metatype type of meta information
*   \param key key of the simulation meta entry
*   \param value of the simulation meta entry
* */
void DBConnector::addSimMetaEntry(int id, std::string metatype, std::string key, std::string value){
    try {
        std::string id_str = std::to_string(id);

        std::string buffer = std::to_string(simId)
                             + '\t' + id_str
                             + '\t' + metatype
                             + '\t' + key
                             + '\t' + value + '\n';
        int r = PQputCopyData(copy_m_cn, buffer.data(), buffer.length());
        checkPostgreSQLCopyError(r, copy_m_cn, "addSimMetaEntry");
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in addSimMetaEntry(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in addSimMetaEntry(...): "  << ex.what() << std::endl;
    }

}

/*!
*   \brief add a time measurement to the database
*   \param timeMeasurementType agentTypeId which the metadata belongs to
*   \param id id of the time measurement
*   \param duration duration of the time measurement in ns
*   \param simTime simtime(tick)
* */
void DBConnector::addTimeMeasurement(std::string timeMeasurementType, int id, long long duration, double simTime){
    try {
        //create needed string objects
        std::string id_str = std::to_string(id);
        std::string duration_str = std::to_string(duration);
        std::string simTime_str = std::to_string(simTime);


        std::string buffer = std::to_string(simId)
                             + '\t' + timeMeasurementType
                             + '\t' + id_str
                             + '\t' + duration_str
                             + '\t' + simTime_str + '\n';
        int r = PQputCopyData(copy_tm_cn, buffer.data(), buffer.length());
        checkPostgreSQLCopyError(r, copy_tm_cn, "addTimeMeasurement");
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in addTimeMeasurement(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in addTimeMeasurement(...): "  << ex.what() << std::endl;
    }
}

//add results

/*!
*   \brief add a seralized result to the Cassandra DB (same method as for string agentID)
*   \param agentId ID of the agent for which a result shall be added
*   \param agentTypeId ID of the type of the agent
*   \param simTime simtime(tick)
*   \param data Pointer to the serialized data to be added
*   \param size size of the data field in bytes
*   \param isFirst if true this is the first time data is added for this agent, false otherwise
* */
void DBConnector::addResultSerialized(int agentId, int agentTypeId, double simTime, void* data, int size, bool isFirst){
    addResultSerialized(std::to_string(agentId), agentTypeId, simTime, data, size, isFirst);
}


/*!
*   \brief add a seralized result to the Cassandra DB
*   \param agentId ID of the agent for which a result shall be added
*   \param agentTypeId ID of the type of the agent
*   \param simTime simtime(tick)
*   \param data Pointer to the serialized data to be added
*   \param size size of the data field in bytes
*   \param isFirst if true this is the first time data is added for this agent, false otherwise
* */
void DBConnector::addResultSerialized(std::string agentId, int agentTypeId, double simTime, void* data, int size, bool isFirst) {

    auto newData = new cass_dataset;
    newData->simId = simId;
    newData->isFirst = isFirst;
    newData->agentId = agentId;
    newData->agentTypeId = agentTypeId;
    newData->data = malloc(size);
    //memcopy data here since function parameter data is freed upon return of this function
    std::memcpy(newData->data, data, size);
    newData->size = size;
    newData->simTime = simTime;

    insertQueueMutex.lock();
    // add new data set to insert queue, it is processed by the cassandra insert thread func
    cassandra_insert_queue.push(newData);
    insertQueueMutex.unlock();
}

/*!
*   \brief Callback function set in addResultSerialized method, This method is called in an asynchronous way by the Cassandra CPP driver
*   \param future Pointer to Cassandra Future object
*   \param data Pointer to respective DBConnector object for this callback (in case of no error)
* */
void cassandraInsertCallback(CassFuture* future, void* data){
    if (cass_future_error_code(future) != CASS_OK) {
        const char* error_message;
        size_t error_message_length;
        cass_future_error_message(future, &error_message, &error_message_length);
        std::string msg = "Could not insert data: ";
        msg += error_message;
        std::cout << "Error in DBConnector [cassandraInsertCallback]: " << msg << std::endl;
        //Throwing an exeption in an asynchronous callback function is not a good practice
        //A DBException thrown here may not be caught by any try/ catch block
        //That is why a simple error message is printed instead to inform the user
        //throw DBException(msg.c_str());
        std::cerr << "Error in DBConnector [cassandraInsertCallback]: " << msg << std::endl;
    }
    //cass_future_free(future);
    DBConnector* dbconn = (DBConnector*) data;
    dbconn->increaseCassandraCallbackCount();
}


/*!
*   \brief end and restart copies to save progress and not lose data on error
* */
void DBConnector::flush(){
    try {
        endPostgreSQLCopy(copy_tm_cn, "timemeasurements", true);
        endPostgreSQLCopy(copy_m_cn, "simulationmetadata", true);
        endPostgreSQLCopy(copy_r_cn, "agentmetadata", true);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in flush(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in flush(...): "  << ex.what() << std::endl;
    }

}

/*!
*   \brief end copies to finalize simulation
* */
void DBConnector::endCopy(){
    try {
        endPostgreSQLCopy(copy_tm_cn, "timemeasurements", false);
        endPostgreSQLCopy(copy_m_cn, "simulationmetadata", false);
        endPostgreSQLCopy(copy_r_cn, "agentmetadata", false);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in endCopy(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in endCopy(...): "  << ex.what() << std::endl;
    }
}

/*!
*   \brief init copy mode for PostgreSQL DB
* */
void DBConnector::initPostgreSQLCopy(){
    try {
        /* Bring COPY connections into copy mode */
        PGresult *r;
                                     
        r = PQexec(copy_tm_cn, "COPY timemeasurements FROM STDIN");
        checkPostgreSQLCopyStartError(r, "connectoworkdb: timemeasurements");
        PQclear(r);
        r = PQexec(copy_m_cn, "COPY simulationmetadata FROM STDIN");
        checkPostgreSQLCopyStartError(r, "connectoworkdb: simulationmetadata");
        PQclear(r);
        r = PQexec(copy_r_cn, "COPY agentmetadata FROM STDIN");
        checkPostgreSQLCopyStartError(r, "connectoworkdb: agentmetadata");
        PQclear(r);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in initPostgreSQLCopy(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in initPostgreSQLCopy(...): "  << ex.what() << std::endl;
    }
}
/*
* functions for actual end or restarts of COPY on connections
*
*/


/*!
*   \brief End (and optionally restart) the result copying for a specific table of the PostgreSQL DB
*   \param p Pointer to PostgreSQL db connection object
*   \param table name of the DB table for which COPY should be ended
*   \param shouldRestart if true COPY is restarted after ending, false otherwise
* */
void DBConnector::endPostgreSQLCopy(PGconn* p, std::string table, bool shouldRestart){
	int r = PQputCopyEnd(p,nullptr);
	if(r == -1){
		throw DBException(PQerrorMessage(p));
	}
	if(r == 0){
		throw DBException("Could not queue end of copy");
	}
	// just r == 1 left
  //TODO
	PGresult* res = PQgetResult(p);
	PQclear(res);
    if(!shouldRestart) {
        return;
    }
    try {
        res = PQexec(p, ("COPY " + table + " FROM STDIN").data());
        checkPostgreSQLCopyStartError(res, std::string("restart copy"));
        PQclear(res);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in endCopy(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in endCopy(...): "  << ex.what() << std::endl;
    }

}

/*
* setter/getter von simid, getCurrentTs and destructor
*
*/


/*!
*   \brief get system time
*   \return system time (time_since_epoch) in microseconds based on chrono library
* */
long long DBConnector::getCurrentTs(){
  return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
}


/*!
*   \brief get simId
*   \return ID of the simulation
* */
int DBConnector::getSimId(){
  return simId;
}


/*!
*   \brief set simId
*   \param _simid simulation ID
* */
void DBConnector::setSimId(int _simId){
  simId=_simId;
}

/*!
*   \brief Destructor of DBConnector object, cleanup, wait for pending asynchronous operations
* */
DBConnector::~DBConnector(){

  // set finished to true so that cassandra insert thread terminates
  finished = true;
  pthread_join(cassandra_insert_thread, nullptr);

  //cassandra
  cass_cluster_free(cluster);
  cass_session_free(session);
  
  //postgres
  PQfinish(conn);
  PQfinish(copy_m_cn);
  PQfinish(copy_r_cn);
  PQfinish(copy_tm_cn);

  insertCountMutex.lock();
  long current_insert_count = insertCount;
  insertCountMutex.unlock();
  callbackCountMutex.lock();
  long current_callback_count = callbackCount;
  callbackCountMutex.unlock();

  //wait for callbacks
  while(current_insert_count > current_callback_count){
      insertCountMutex.lock();
      current_insert_count = insertCount;
      insertCountMutex.unlock();

      callbackCountMutex.lock();
      current_callback_count = callbackCount;
      callbackCountMutex.unlock();
  }
}