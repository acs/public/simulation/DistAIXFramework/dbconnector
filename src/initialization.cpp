/**
 * This file is part of DistAIX DBconnector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "DBConnector.h"
#include "DBException.h"

/*
When used by multiple threads, only execute it once!
*/
/*!
*   \brief creates database and tables if not exist, when using multiple threads it does only need to be executed once (but all threads need to call switchToWorkDB to use the correct database)
* */
void DBConnector::initializePostgreSQLDB(){
  createPostgreSQLDatabase();
  setupPostgreSQLDB();
  PostgreSQLisSetup = true; // TODO: redundant?
  createPostgreSQLSimTable();
  createPostgreSQLResultTypeTable();
  createPostgreSQLTimeMeasurementTypeTable();
  createPostgreSQLTimeMeasurementTable();
  createPostgreSQLSimMetaTable();
  createPostgreSQLAgentMetaTable();
}


/*!
*   \brief setup keyspace and tables
* */
void DBConnector::setupCassandra() {
  createCassandraKeyspace();
  createCassandraTable();
}

/*!
*   \brief create cassandra keyspace if it doesn't exist
* */
void DBConnector::createCassandraKeyspace(){
    try {

        std::string query = "CREATE KEYSPACE IF NOT EXISTS " + cass_db +
                            " WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '1' };";
        CassStatement *statement = cass_statement_new(query.c_str(), 0);
        CassFuture *result_future = cass_session_execute(session, statement);
        if (cass_future_error_code(result_future) != CASS_OK) {
            checkCassandraFuture(result_future, "Query for keyspace creation failed: ");
        }
        cass_future_free(result_future);
        cass_statement_free(statement);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in createCassandraKeyspace(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in createCassandraKeyspace(...): "  << ex.what() << std::endl;
    }
}


/*!
*   \brief create results and agents tables
* */
void DBConnector::createCassandraTable(){
    try {
        std::string query = "CREATE TABLE IF NOT EXISTS " + cass_db + ".results (simid int, simtime double, agentid text, \
                        agenttypeid int,  result blob, PRIMARY KEY (agentid, simid,simtime))";

        CassStatement *statement = cass_statement_new(query.c_str(), 0);
        CassFuture *result_future = cass_session_execute(session, statement);
        if (cass_future_error_code(result_future) != CASS_OK) {
            checkCassandraFuture(result_future, "Query for table creation failed: ");
        }
        cass_future_free(result_future);
        cass_statement_free(statement);

        query = "CREATE TABLE IF NOT EXISTS " + cass_db + ".agents (simid int, agentid text, PRIMARY KEY (simid, agentid))";

        statement = cass_statement_new(query.c_str(), 0);
        result_future = cass_session_execute(session, statement);
        if (cass_future_error_code(result_future) != CASS_OK) {
            checkCassandraFuture(result_future, "Query for table creation failed: ");
        }
        cass_future_free(result_future);
        cass_statement_free(statement);

    }catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in createCassandraTable(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in createCassandraSession(...): "  << ex.what() << std::endl;
    }
}

/*!
*   \brief create a cassandra session
* */
void DBConnector::createCassandraSession() {
    try {
        cluster = cass_cluster_new();
        session = cass_session_new();

        cass_cluster_set_contact_points(cluster, cass_hosts.c_str());
        cass_cluster_set_num_threads_io(cluster, iothreads);

        CassFuture *connect_future = cass_session_connect(session, cluster);
        if (cass_future_error_code(connect_future) != CASS_OK) {
            checkCassandraFuture(connect_future, "Cannot connect to Cassandra: ");
        }
        cass_future_free(connect_future);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in createCassandraSession(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in createCassandraSession(...): "  << ex.what() << std::endl;
    }
}


/* Following functions use synchronous connections and partly do use query results */

/* 
*Database setup functions 
*
*/

/*!
*   \brief create PostgreSQL data base
* */
void DBConnector::createPostgreSQLDatabase(){
    try {
        PGresult *r = PQexec(conn,
                             ("SELECT COUNT(datname) FROM pg_database WHERE datname = '" + database + "'").c_str());
        checkPostgreSQLSelectError(r, "createPostgreSQLDatabase_exists");
        int c = std::stoi(PQgetvalue(r, 0, 0));
        PQclear(r);
        if (c == 0) {
            r = PQexec(conn, ("CREATE DATABASE " + database).c_str());
            checkPostgreSQLError(r, "createPostgreSQLDatabase_create");
            PQclear(r);
        }
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in createPostgreSQLDatabase(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in createPostgreSQLDatabase(...): "  << ex.what() << std::endl;
    }
}

/*!
*   \brief connect to the correct database, sets up copy connections and creates the prepared statement for cassandra, needs to be executed by all threads
* */
void DBConnector::setupPostgreSQLDB(){
  if(PostgreSQLisSetup) //if already setup, no need to this
    return;
  
  PostgreSQLisSetup=true;
  if(PQstatus(conn) == CONNECTION_OK){
    PQfinish(conn);
  }

 
  std::string buffer =  "dbname="+database+" user="+username+" password="+password+" hostaddr="+host+" port="+ std::to_string(port);
  conn       = PQconnectdb(buffer.c_str()); 
  copy_tm_cn = PQconnectdb(buffer.c_str()); 
  copy_m_cn  = PQconnectdb(buffer.c_str()); 
  copy_r_cn  = PQconnectdb(buffer.c_str()); 

  if(PQstatus(conn) != CONNECTION_OK
	|| PQstatus(copy_tm_cn) != CONNECTION_OK
	|| PQstatus(copy_m_cn) != CONNECTION_OK
	|| PQstatus(copy_r_cn) != CONNECTION_OK){
    throw DBException("Connection to DB failed");
  }
}

/*!
*   \brief prepare a query to the Cassandra DB
* */
void DBConnector::prepareCassandraQuery(){
    try {
        std::string query = "INSERT INTO ";
        query += cass_db;
        query += ".results (simid, simtime, agentid, \
          agenttypeid, result) VALUES(?,?,?,?,?)"; //simtime, agentid, agenttypeid, result

        CassFuture *prepare_future = cass_session_prepare(session, query.c_str());

        if (cass_future_error_code(prepare_future) != CASS_OK) {
            checkCassandraFuture(prepare_future, "Could not create prepared cassandra statement");
        }

        prepared = cass_future_get_prepared(prepare_future);
        cass_future_free(prepare_future);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in prepareCassandraQuery(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in prepareCassandraQuery(...): "  << ex.what() << std::endl;
    }
}

/*
* Database Table create functions
*
*/

/*!
*   \brief Create table of simulation meta entries in PostgreSQL DB
* */
void DBConnector::createPostgreSQLSimMetaTable(){
    try {
        PGresult *r = PQexec(conn,
                             "CREATE TABLE IF NOT EXISTS simulationmetadata(simulationId serial references simulations(simulationId) NOT NULL, "\
          "id BIGINT, metatype text, key text, value text)");
        checkPostgreSQLError(r, "createPostgreSQLMetaTable");
        PQclear(r);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in createPostgreSQLSimMetaTable(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in createPostgreSQLSimMetaTable(...): "  << ex.what() << std::endl;
    }
}

/*!
*   \brief Create table of simulations in PostgreSQL DB
* */
void DBConnector::createPostgreSQLSimTable(){
    try {
        PGresult *r = PQexec(conn,
                             "CREATE TABLE IF NOT EXISTS simulations( simulationId serial PRIMARY KEY, start_ts timestamp, stop_ts timestamp)");
        checkPostgreSQLError(r, "createPostgreSQLSimTable");
        PQclear(r);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in createPostgreSQLSimTable(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in createPostgreSQLSimTable(...): "  << ex.what() << std::endl;
    }
}

/*!
*   \brief Create table of result types in PostgreSQL DB
* */
void DBConnector::createPostgreSQLResultTypeTable(){
    try {
        PGresult *r = PQexec(conn, "CREATE TABLE IF NOT EXISTS resultTypes(resultType text NOT NULL , " \
    "simulationid integer references simulations(simulationid), unit text, description text, PRIMARY KEY(resultType, simulationid))");
        checkPostgreSQLError(r, "createPostgreSQLResultTypeTable");
        PQclear(r);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in createPostgreSQLResultTypeTable(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in createPostgreSQLResultTypeTable(...): "  << ex.what() << std::endl;
    }
}

/*!
*   \brief Create table of agent meta entries in PostgreSQL DB
* */
void DBConnector::createPostgreSQLAgentMetaTable(){
    try {
        PGresult *r = PQexec(conn,
                             "CREATE TABLE IF NOT EXISTS agentmetadata(simulationId serial references simulations(simulationId) NOT NULL, " \
          "metatype text NOT NULL, " \
          "agentId text, agentTypeId bigint, value float NOT NULL)");

        checkPostgreSQLError(r, "createPostgreSQLMetaTable");
        PQclear(r);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in createPostgreSQLAgentMetaTable(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in createPostgreSQLAgentMetaTable(...): "  << ex.what() << std::endl;
    }
}

/*!
*   \brief Create table of time measurements in PostgreSQL DB
* */
void DBConnector::createPostgreSQLTimeMeasurementTable(){
    try {
        PGresult *r = PQexec(conn,
                             "CREATE TABLE IF NOT EXISTS timemeasurements(simulationId serial references simulations(simulationId) NOT NULL, " \
          "timeMeasurementType text references timemeasurementtypes(timeMeasurementType), id bigint, duration_ns bigint, simtime decimal)");
        checkPostgreSQLError(r, "createPostgreSQLTimeMeasurementTable");
        PQclear(r);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in createPostgreSQLTimeMeasurementTable(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in createPostgreSQLTimeMeasurementTable(...): "  << ex.what() << std::endl;
    }

}

/*!
*   \brief Create table of time measurement types in PostgreSQL DB
* */
void DBConnector::createPostgreSQLTimeMeasurementTypeTable(){
    try {
        PGresult *r = PQexec(conn,
                             "CREATE TABLE IF NOT EXISTS timemeasurementtypes(timeMeasurementType text NOT NULL PRIMARY KEY, description text)");
        checkPostgreSQLError(r, "createPostgreSQLTimeMeasurementTypeTable");
        PQclear(r);
    }
    catch(const DBException &ex){
        std::cerr << "ERROR: DBConnector caught DBException in createPostgreSQLTimeMeasurementTypeTable(...): "  << ex.what() << std::endl;
        std::cout << "ERROR: DBConnector caught DBException in createPostgreSQLTimeMeasurementTypeTable(...): "  << ex.what() << std::endl;
    }
}