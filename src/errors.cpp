/**
 * This file is part of DistAIX DBconnector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "DBConnector.h"
#include <string>
#include <iostream>
#include "DBException.h"

/*!
*   \brief Check PostgreSQL result for error, throw exception in case error occurred
*   \param r PostgreSQL result to be checked
*   \param info Information to be used in error message
* */
void DBConnector::checkPostgreSQLError(PGresult* r, std::string info){
  if(PQresultStatus(r) == PGRES_COMMAND_OK) return;
  throw DBException(info+' '+PQerrorMessage(conn));
}

/*!
*   \brief Check PostgreSQL result for an error upon SELECT operation, throw exception in case error occurred
*   \param r PostgreSQL result to be checked
*   \param info Information to be used in error message
* */
void DBConnector::checkPostgreSQLSelectError(PGresult* r, std::string info){
  if(PQresultStatus(r) == PGRES_TUPLES_OK) return;
  throw DBException(info+' '+PQerrorMessage(conn));
}

/*!
*   \brief Check PostgreSQL result for an error upon COPY START operation, throw exception in case error occurred
*   \param r PostgreSQL result to be checked
*   \param info Information to be used in error message
* */
void DBConnector::checkPostgreSQLCopyStartError(PGresult *r, std::string info){
  if(PQresultStatus(r) == PGRES_COPY_IN) return;
  throw DBException(info+' '+PQerrorMessage(conn));
}

/*!
*   \brief Check copy error of PostgreSQL DB
*   \param r Integer indicating error status
*   \param p Pointer to PostgreSQL connector
*   \param info Information to be used in error message
* */
void DBConnector::checkPostgreSQLCopyError(int r, PGconn* p, std::string info){
  //success
  if(r == 1) return;
  if(r == 0){
    throw DBException("could not queue data");
  }
  if(r == -1){
    std::cout << "ERROR: " << PQerrorMessage(p) << std::endl;
    std::cout << "Info:  " << info << std::endl;
    throw DBException(PQerrorMessage(p));
  }
}

/*!
*   \brief Check PostgreSQL result for error (busy waiting until result is obtained!)
*   \param c Pointer to PostgreSQL connector
* */
void DBConnector::checkPostgreSQLPQresult(PGconn* c){
	PGresult * res = PQgetResult(c);
	while(res != nullptr){
		if(PQresultStatus(res) != PGRES_COMMAND_OK)
			std::cout << "Ended copy: "<<PQresStatus(PQresultStatus(res)) << " " << PQresultErrorMessage(res) << std::endl;
		PQclear(res);
		res = PQgetResult(c);
	}
}


/*!
*   \brief Create an error message and throw an exception in case of an error with a Cassandra Future object
 *  \param cassf Poiter to the Cassandra Future object for which an error occured
 *  \param msg String to be contained in error message on terminal
* */
void DBConnector::checkCassandraFuture(CassFuture* cassf, std::string msg){
    const char* error_message;
    size_t error_message_length;
    cass_future_error_message(cassf, &error_message, &error_message_length);
    msg += error_message;
    std::cout << "Error: " << msg << std::endl;
    throw DBException(msg.c_str());
}