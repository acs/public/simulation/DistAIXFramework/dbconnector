/**
 * This file is part of DistAIX DBconnector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include <DBConnector.h>
#include <DBException.h>
#include <message.pb.h>

#define CASSANDRA "cassandra"
#define CASSANDRA_DATABASE "example"
#define IOTHREADS 4

#define POSTGRES "172.19.0.3"
#define POSTGRES_USER "postgres"
#define POSTGRES_PASSWORD "postgres"
#define POSTGRES_PORT 5432
#define POSTGRES_DATABASE "example"

#define AGENTID 5
#define AGENTTYPEID 25


//Additional method currently not of any value
//resulttype, agentid, agenttypeid, value
//dbconn->addAgentMetaEntry("powerplant", "5", "25", 12.3);

void setup(DBConnector* dbconn);
void addMetadata(DBConnector* dbconn);
void addTimeMeasurements(DBConnector* dbconn);
void addResult(DBConnector* dbconn);
void finishSimulation(DBConnector* dbconn);
    
int main(){
    try{
        DBConnector* dbconn = new DBConnector(POSTGRES, POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_PORT, POSTGRES_DATABASE);
        setup               (dbconn);
        addMetadata         (dbconn);
        addTimeMeasurements (dbconn);
        addResult           (dbconn);
        finishSimulation    (dbconn);
        delete dbconn;
    }catch(DBException e){
        std::cout << "Error: " << e.what() << std::endl;
    }
    return 0;
}

void setup(DBConnector* dbconn){
    dbconn->initialize();
    dbconn->setup();
    dbconn->initCopy();
    int simid = dbconn->newSim(); //when using multiple threads, only call on, otherwise you get 4 different simids
    //instead call it in one thread, share the simid between threads and use dbconn->setSimId(simid)
    dbconn->setSimId(simid);

    std::cout << "Simulation ID: " << simid << std::endl;

    //setup cassandra
    dbconn->setCassandraConfig(CASSANDRA, CASSANDRA_DATABASE, IOTHREADS);
    dbconn->createCassandraSession();
    dbconn->setupCassandra(); // same here, only execute in one thread
    dbconn->prepareCassandraQuery();
}


void addMetadata(DBConnector* dbconn){
    int _rank = 0;
    std::string _metatype = "agents on ranks";
    std::string _key = "agents";
    std::string _value = "25";
    dbconn->addSimMetaEntry(_rank, _metatype, _key, _value);
}

void addTimeMeasurements(DBConnector* dbconn){
    //register
    dbconn->addTimeMeasurementType("simulation-time",
                                               "Measure after db initialize methods till desctructor of model");
    dbconn->addTimeMeasurementType("synctime", "Time for syncing Agents");
    //measure stuff...
    //add it
    //name, id (usually a rank in our case), duration (measured time), simtime (time of simulation when we measured it)
    dbconn->addTimeMeasurement("simulation-time", 0, 12.0,   1.0);
    dbconn->addTimeMeasurement("synctime",        0, 1235.2, 1.0);

}
 
void addResult(DBConnector* dbconn){
    bool isFirst = true;

    Powerplant p;

    //name, unit, description
    dbconn->addResultType(p.descriptor()->field(0)->name(), "W", "P_min");
    dbconn->addResultType(p.descriptor()->field(1)->name(), "W", "P_min");

    for(double simtime=0; simtime<25; simtime++){
        p.set_id(25);
        p.set_capacity(123.45);

        int size = p.ByteSize();
        void* buffer = malloc(size);
        p.SerializeToArray(buffer, size);

        dbconn->addResultSerialized(AGENTID, AGENTTYPEID, simtime, buffer , size, isFirst);
        isFirst = false;
    }
}

void finishSimulation(DBConnector* dbconn){
    dbconn->finishSimulation(); //only execute once when using multiple threads
    dbconn->endCopy();
}
