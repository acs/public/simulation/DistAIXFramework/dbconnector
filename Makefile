#############################################################################
#
# This file is part of DistAIX DBconnector
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

CC      := g++
OBJ     := $(patsubst src/%.cpp,obj/%.o,$(wildcard src/*.cpp))
CCFLAGS +=-g -std=c++0x -I./include -Wall -I/usr/local/include/ -I/usr/include/postgresql/ -I/global/cluster/centos/cassandra-cpp-driver/include
LDFLAGS +=-lpq -L/usr/lib/x86_64-linux-gnu/ -L/usr/lib/ 
LIBNAME := libdbconnector.so
OBJDIR  := obj

all: buildlib

buildlib: $(LIBDIR) $(LIBNAME)

$(OBJDIR):
	mkdir -p $(OBJDIR)

$(OBJDIR)/%.o: src/%.cpp
	$(CC) -c -fPIC $(CCFLAGS) -o $@ $<

$(LIBNAME): $(OBJDIR) $(OBJ)
	gcc -shared -Wl,-soname,$(LIBNAME) -o $(LIBNAME) obj/*.o

clean:
	rm -rf $(OBJDIR) $(LIBNAME)

.PHONY: clean buildlib
