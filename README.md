# DistAIX DBConnector Library

[![pipeline status](https://git.rwth-aachen.de/acs/research/swarmgrid/dbconnector/badges/master/pipeline.svg)](https://git.rwth-aachen.de/acs/research/swarmgrid/dbconnector/commits/master)

The DistAIX DBConnector Library is an interface between the [DistAIX simulator](https://git.rwth-aachen.de/acs/research/swarmgrid/distaix) and a database system for simulation result storage.
It provides all required interface methods to connect a parallel and distributed DistAIX simulation to a remote database system and store data.
The database system consists of a PostgreSQL data base for the meta data of a simulation and a Cassandra database cluster for time series data generated at each simulation time step.

## Dependencies
The library has the following dependencies
- Cassandra CPP driver 
- PostgreSQL library
- CMake 

## Compiling
Use CMake to compile the library on your system:

```
mkdir build
cd build
cmake ..
make
```

## Setup and Structure of Databases
For information about the configuration and structure of the PostgreSQL and Cassandra databases, it is recommended to read the [respective documentation page of DistAIXweb](https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/distaixweb/blob/master/doc/README_deploy.md).

## Usage

The DistAIX simulator uses the DBConnector by instantiating one DBConnector object in each simulation process.
That way, all functionality provided by the library is available in the simulator.
Each `Model`, `Agent` and `Edge` object of a DistAIX simulation uses an instance of a DBConnector object to store meta information (= simulation time invariant data) about itself and time series data in the data base system.
We provide an example to get started with the DBConnector Library independent of DistAIX.
 
### Using the example
Change to example/

The dockerfile contains all dependencies you need.
I recommend you use docker-compose:

```
docker-compose up -d
```

It will build the image and start postgres & cassandra. (That might take some time)
The container mounts the current directory so you can work with all necessary tools.

Attach to the container with
```
docker-compose exec build_env /bin/bash
```

Now, first you need to find out the IP address of postgres, because for some reason the postgres driver is not capable of doing that.

```
ping postgres
```

and copy the IP address you see there.

Now head over to example/main.cpp and replace POSTGRES definiton with your ip.


#### Building
run `make` in example

#### Running
use `./run.sh` in example


### Code Documentation
You can use the Doyxfile provided in the `docs` folder to create an HTML documentation of the code.


## Copyright

2022, Institute for Automation of Complex Power Systems, EONERC  

## License

This project is released under the terms of the [GPL version 3](COPYING.md).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

- [Felix Wege](mailto:felix.wege@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)  
