#############################################################################
# DBconnector Ubuntu 18.04 Dockerfile
#
# This Dockerfile builds an image based on Ubuntu 18.04 which contains all dependencies
# to build DBconnector
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################
FROM ubuntu:18.04

ARG GIT_REV=unknown
ARG GIT_BRANCH=unknown
ARG VERSION=unknown
ARG VARIANT=unknown

ENV DEBIAN_FRONTEND=noninteractive

# Toolchain
RUN apt-get update && apt-get install -y \
	git cmake g++ libtool pkg-config openssl

# Dependencies of DBconnector Library
RUN apt-get update && apt-get install -y \
    libuv1-dev libssl-dev libpq-dev \
    postgresql postgresql-contrib postgresql-server-dev-10

# Install Cassandra cpp driver (Dependency of DBConnector Library)
RUN cd /tmp && mkdir cass && cd cass && \
    git clone https://github.com/datastax/cpp-driver.git . && \
    git checkout 2.10.0 && \
    mkdir build && cd build && \
    cmake .. && make -j$(nproc) && make install && \
    ldconfig && \
    rm -rf /tmp/*

# set library environment variables
ENV LD_LIBRARY_PATH="/usr/local/lib:/usr/local/lib/x86_64-linux-gnu:/usr/local/lib64:/usr/lib"
ENV LIBRARY_PATH="/usr/local/lib:/usr/local/lib/x86_64-linux-gnu:/usr/local/lib64:/usr/lib"
ENV PATH="usr/local/bin:${PATH}"

WORKDIR /dbconnector
ENTRYPOINT bash

LABEL \
	org.label-schema.schema-version="1.0" \
	org.label-schema.name="DBconnector" \
	org.label-schema.license="GPL-3.0" \
	org.label-schema.vcs-ref="$GIT_REV" \
	org.label-schema.vcs-branch="$GIT_BRANCH" \
	org.label-schema.version="$VERSION" \
	org.label-schema.variant="$VARIANT" \
	org.label-schema.vendor="Institute for Automation of Complex Power Systems, RWTH Aachen University" \
	org.label-schema.description="An image containing all build-time dependencies for DBconnector based on Ubuntu" \
	org.label-schema.vcs-url="https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/dbconnector"