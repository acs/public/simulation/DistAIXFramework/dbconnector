/**
 * This file is part of DistAIX DBconnector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/
#ifndef DBCONNECTOR_H
#define DBCONNECTOR_H

#include <libpq-fe.h>
#include <string>
#include <cassandra.h>
#include <mutex>
#include <pthread.h>
#include <queue>

typedef struct{
    long simId;
    std::string agentId;
    int agentTypeId;
    double simTime;
    void * data;
    int size;
    bool isFirst;
} cass_dataset;

typedef struct {
    std::string* cass_db;
    CassSession** session;
    const CassPrepared** prepared;
    long* insertCount_pointer;
    std::queue<cass_dataset*> * cassandra_insert_queue;
    bool * finished;
    void * pointer_to_dbconn;
    std::mutex * insertQueueMutex;
    std::mutex * insertCountMutex;
}
cassinfo;



class DBConnector{
private:

  long simId; //!< ID of a simulation to be used as identifier in DB

  /* PostgreSQL related members */
  PGconn* conn; //!< PostgreSQL connector
  std::string host; //!< IP of PostgreSQL DB
  std::string username; //!< Username for PostgreSQL DB
  std::string password; //!< Password for PostgreSQL DB
  std::string database; //! Name of PostgreSQL DB
  int port; //!< Port of PostgreSQL DB
  bool PostgreSQLisSetup = false; //!< true if PostgreSQL DB has been set up by DBConnector
  long resultCount = 0; //!< Counting agent meta entries to PostgreSQL DB
  /*
   * COPY connections
   */
  PGconn* copy_tm_cn; //!< PostgreSQL copy connector for time measurements
  PGconn* copy_m_cn; //!< PostgreSQL copy connector for simulation meta data
  PGconn* copy_r_cn;  //!< PostgreSQL copy connector for agent meta data

  /* Cassandra related members */
  std::string cass_hosts; //!< String list of Cassandra hosts
  std::string cass_db; //!< Name of cassandra DB
  CassCluster* cluster = nullptr; //!< Cassandra cluster configuration
  CassSession* session = nullptr; //!< Cassandra session
  const CassPrepared* prepared;  //!< Cassandra statement that has been prepared on cluster side (pre-parsed and cached)
  std::mutex insertCountMutex; //!< mutex for insert counter
  std::mutex callbackCountMutex; //!< mutex for callback counter
  std::mutex insertQueueMutex; //!< mutex for insert queue
  long insertCount=0; //!< count how many inserts are issued
  long callbackCount=0; //!< count how many callbacks were called
  int iothreads = 4; //!< Number of Cassandra IO Threads (threads that handle query requests)
  pthread_t cassandra_insert_thread;
  cassinfo cassandra_info_for_thread;
  std::queue<cass_dataset*> cassandra_insert_queue;
  bool finished;

  /* Private PostgreSQL related methods */
  void createPostgreSQLDatabase();
  void createPostgreSQLSimTable();
  void createPostgreSQLResultTypeTable();
  void createPostgreSQLAgentMetaTable();
  void createPostgreSQLSimMetaTable();
  void createPostgreSQLTimeMeasurementTable();
  void createPostgreSQLTimeMeasurementTypeTable();
  void checkPostgreSQLCopyError(int r, PGconn* p, std::string info);
  void checkPostgreSQLCopyStartError(PGresult* r, std::string info);
  void checkPostgreSQLError(PGresult* r, std::string info);
  void checkPostgreSQLSelectError(PGresult* r, std::string info);
  void endPostgreSQLCopy(PGconn* p, std::string table, bool shouldRestart);

    /* Private Cassandra related methods */
  //void addAgentToCassandra(std::string agentId);
  void createCassandraTable();
  void createCassandraKeyspace();
  void checkCassandraFuture(CassFuture* cassf, std::string msg);

  /* General methods */
  long long getCurrentTs();
public:
  DBConnector(std::string _host, std::string _username, std::string _password, int _port, std::string _database);
  ~DBConnector();
  int newSim();
  void initPostgreSQLCopy();
  void setupPostgreSQLDB();
  void setupCassandra();
  void prepareCassandraQuery();
  void setCassandraConfig(std::string hosts, std::string dbname, int _iothreads);
  void createCassandraSession();
  void increaseCassandraCallbackCount();
  void addResultType(std::string name, std::string unit, std::string description);
  void addAgentMetaEntry(std::string resultType, long agentId, long agentTypeId, double value);
  void addAgentMetaEntry(std::string resultType, std::string agentId, long agentTypeId, double value);
  void addResultSerialized(int agentId, int agentTypeId, double simTime, void* data, int size, bool isFirst);
  void addResultSerialized(std::string agentId, int agentTypeId, double simTime, void* data, int size, bool isFirst);
  void addSimMetaEntry(int id, std::string metatype, std::string key, std::string value);
  void addTimeMeasurement(std::string TimeMeasurementType, int id, long long duration, double simTime);
  void addTimeMeasurementType(std::string mt, std::string description);
  void finishSimulation();
  void endCopy();
  void initializePostgreSQLDB();
  void flush();

  void checkPostgreSQLPQresult(PGconn* c);
  int getSimId();
  void setSimId(int _simId);
};


#endif
