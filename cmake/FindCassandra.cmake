#############################################################################
# DBconnector find Cassandra ccp-driver package
# This module defines
# Cassandra_INCLUDE_DIRS, where to find cassandra.h, etc.
# Cassandra_LIBRARIES, the library to link against to use cassandra.
# Cassandra_FOUND, If false, don't try to use cassandra.
#
# This file is part of DistAIX DBconnector
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

find_library(Cassandra_LIBRARY NAMES cassandra)
find_library(Libuv_LIBRARY NAMES uv libuv)

find_path(Cassandra_INCLUDE_DIR NAMES cassandra.h )
find_path(Libuv_INCLUDE_DIR NAMES uv.h)

set(Cassandra_LIBRARIES ${Cassandra_LIBRARY} ${Libuv_LIBRARY} )
set(Cassandra_INCLUDE_DIRS ${Cassandra_INCLUDE_DIR} ${Libuv_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments
find_package_handle_standard_args(Cassandra DEFAULT_MSG Cassandra_LIBRARIES Cassandra_INCLUDE_DIRS)

# if the include and the library are found then we have it
if (Cassandra_INCLUDE_DIRS AND Cassandra_LIBRARIES)
    SET( Cassandra_FOUND 1 )
endif()

MARK_AS_ADVANCED(
        Cassandra_INCLUDE_DIRS
        Cassandra_LIBRARIES
)