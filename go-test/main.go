// This file is part of DistAIX DBconnector
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// *********************************************************************************

package main

import (
	"fmt"
	"log"

	"github.com/gocql/gocql"
)

func main() {
	cluster := gocql.NewCluster("137.226.248.96", "137.226.248.86", "137.226.248.89")
	cluster.Keyspace = "swarmgrid"

	session, err := cluster.CreateSession()
	if err != nil {
		log.Fatal(err)
	}
	//query data
	simID := 60
	agentIDs := []string{"151", "223", "5_6", "-2001"}
	start := 0.0
	stop := 120.0
	GetResults(session, simID, agentIDs, start, stop)
}

func GetResults(session *gocql.Session, simID int, agentIDs []string, start float64, stop float64) {
	q := `SELECT agentid, agenttypeid, simtime, result FROM results WHERE simid=? AND agentid IN ? AND simtime >= ? AND simtime <= ?`
	iter := session.Query(q, simID, agentIDs, start, stop).Iter()

	var agentid string
	var agenttypeid float64
	var simtime float64
	var result []byte

	formatString := "|%15s|%15f|%15f|%15s|\n"
	formatHeaderString := "|%15s|%15s|%15s|%15x|\n"

	fmt.Printf(formatHeaderString, "AgentID", "AgentTypeID", "Simtime","Bytes")
	for iter.Scan(&agentid, &agenttypeid, &simtime, &result) {
		fmt.Printf(formatString, agentid, agenttypeid, simtime,result)
	}

	if err := iter.Close(); err != nil {
		log.Fatal(err)
	}
}
